$(function () {

    $("#datepicker").datepicker({
        nextText: "",
        prevText: "",
        dayNamesMin: ["S", "M", "T", "W", "T", "F", "S"],
        beforeShowDay: function (date) {
            let holidays = ['19', '20', '21'];
            let day = $.datepicker.formatDate("d", date);
            let index = holidays.indexOf(day);

            if (index === -1) {
                return [true, '']
            }
            if (index === 0) {
                return [true, 'holidays start-holidays']
            }
            if (index < holidays.length - 1) {
                return [true, 'holidays middle-holidays']
            }
            if (index === holidays.length - 1) {
                return [true, 'holidays end-holidays']
            }
        }
    });

    var date = $.datepicker.formatDate("d MM yy", new Date());
    $('.header-current-date').html(date);
});