var gulp = require ('gulp');
var sass = require ('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;

var paths = {
  html:['*.html'],
  sass:['./src/**/*.scss'],
  script:['script.coffee']
};

gulp.task('sass', function (){
  gulp.src('./src/style.scss')
  .pipe(sourcemaps.init())
  .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(sourcemaps.write('../maps'))
	.pipe(gulp.dest('./dest'))
	.pipe(reload({stream:true}));
});

gulp.task('html', function (){
	gulp.src('*.html')
	.pipe(reload({stream:true}));
});

gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: "./"
    },
    port: 9080,
    open: true,
    notify: false
  });
});

gulp.task('watcher',function(){
  gulp.watch(paths.sass, ['sass']);
  gulp.watch(paths.html, ['html']);
  // ...
  // ...
});

gulp.task('default', ['watcher', 'browserSync']);